#!/bin/sh

# This file is part of secret_service.

# secret_service is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# secret_service is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with secret_service.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2020-2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License


trap 'exit 128' INT
export PATH


erun() {
    echo "[RUNNING]: ${*}"
    if "${@}"
    then
        echo "[SUCCESS]: ${*}"
    else
        echo "[FAILURE]: ${*}"
    fi
}


if [ -n "${1}" ] && [ -d "${1}" ]
then
    cd "${1}" || exit 1
    source_dir="$(pwd)"
else
    source_dir="${HOME}/source"
fi

echo "source_dir: ${source_dir}"


# Closed (non-public)

echo "--- Closed ---"

erun mkdir -p "${source_dir}"/closed/localhost/xgqt


# Public

echo "--- Public ---"

erun mkdir -p "${source_dir}"/public
cd "${source_dir}"/public || exit 1

erun mkdir -p ~/Public
erun ln -ns "${source_dir}"/public ~/Public/source

erun mkdir -p git.savannah.gnu.org
erun mkdir -p git.sr.ht/\~xgqt
erun mkdir -p github.com/xgqt
erun mkdir -p gitlab.com/src_prepare
erun mkdir -p gitlab.com/xgqt
erun mkdir -p gitlab.gnome.org
erun mkdir -p gitweb.gentoo.org/proj
erun mkdir -p invent.kde.org

erun ln -ns github.com/xgqt        secondary
erun ln -ns gitlab.com/xgqt        primary

erun ln -ns git.savannah.gnu.org   savannah
erun ln -ns git.sr.ht              sourcehut
erun ln -ns github.com             github
erun ln -ns gitlab.com             gitlab
erun ln -ns gitlab.gnome.org       gnome
erun ln -ns invent.kde.org         kde


# Overlays

echo "--- Overlays ---"

erun mkdir -p "${source_dir}"/overlays

cd "${source_dir}"/public/gitlab.com/src_prepare || exit 1
erun git clone --recursive --verbose git@gitlab.com:src_prepare/src_prepare-overlay.git

cd "${source_dir}"/public/gitlab.com/xgqt || exit 1
erun git clone --recursive --verbose git@gitlab.com:xgqt/crossdev-overlay.git

cd "${source_dir}"/public/gitlab.com/xgqt || exit 1
erun git clone --recursive --verbose git@gitlab.com:xgqt/myov.git

cd "${source_dir}"/public/gitweb.gentoo.org/proj || exit 1
erun git clone --recursive --verbose git@git.gentoo.org:repo/proj/guru.git

cd "${source_dir}"/overlays || exit 1
erun ln -ns ../public/gitlab.com/src_prepare/src_prepare-overlay .
erun ln -ns ../public/gitlab.com/xgqt/myov .
erun ln -ns ../public/gitlab.com/xgqt/crossdev-overlay crossdev
erun ln -ns ../public/gitweb.gentoo.org/proj/guru .
