# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION=""
HOMEPAGE=""

if [[ "${PV}" == *9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI=""
else
	SRC_URI=""
	KEYWORDS="~amd64"
fi

RESTRICT="
	mirror
	!test? ( test )
"
LICENSE=""
SLOT="0"
IUSE="test"

BDEPEND=""
DEPEND=""
RDEPEND=""
