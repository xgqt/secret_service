#!/bin/sh


# This file is part of secret_service.

# secret_service is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# secret_service is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with secret_service.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2020-2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License

# Clone my stuff


if ssh -T git@gitlab.com
then
    echo "Authenticated"
    remote_host="git@gitlab.com:xgqt"
else
    echo "Unauthenticated"
    remote_host="https://gitlab.com/xgqt"
fi


repo_list="
aet
autopartition
awesome-config
blog
chrome
cipherutils
conkies
crossdev-overlay
dice
e-exp
for_ansible
funstrap
genkerneler
genlica
genstrap
gentoo-mini-mirror
kerneler
mydot
myov
mysless
nf
pystow
qrfetch
rktpet
rktsay
secret_service
startpage
tormngr
tree-ng
user.js
wmacs
wxmpd
xgqt.gitlab.io
"


# If in <this repo's name> go up

if [ "${PWD##*/}" = secret_service ]
then
    cd ..
fi


for repo in ${repo_list}
do
    if [ -n "${repo}" ]
    then
        if [ -d "${repo}" ]
        then
            echo "Exists: ${repo}"
        else
            echo "Pulling: ${repo}"
            git clone --recursive --verbose "${remote_host}/${repo}"
        fi
    fi
done
