#!/bin/sh


# This file is part of secret_service.

# secret_service is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# secret_service is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with secret_service.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2020-2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License


here_dir="$(pwd)"
templates_dir="${HOME}/Templates"


if [ -d "${templates_dir}" ]
then
    echo "$templates_dir exists, overwrite?"
    read -r if_overwrite
    case ${if_overwrite} in
        [Yy]* )
            rm -rfdv "${templates_dir}"
            ;;
        [Nn]* )
            exit 1
            ;;
        * )
            echo "Please answer Yes or No"
            exit 1
            ;;
    esac
fi

cd "${HOME}" || exit 1

ln -vsf "${here_dir}"/Templates "${templates_dir}"
