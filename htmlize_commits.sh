#!/bin/sh


# This file is part of secret_service.

# secret_service is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# secret_service is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with secret_service.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2020-2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License


repo=$(git config --get remote.origin.url | sed -e 's/http:\/\///g' -e 's/https:\/\///g' -e 's/git@//g' -e 's/:/\//g' -e 's/\.git//g')

html_file=/tmp/$(echo "$repo" | sed -e 's/\//_/g')-commits.html

commits=$(git rev-list --count HEAD)
first_commit_date=$(git log --reverse --format=%cd | sed 1q)
last_commit_date=$(git log --reverse -1 --format=%cd)
changes=$(git status --porcelain)


# Top

echo "
<!DOCTYPE html>
<html>
	<head>
		<title>${repo}</title>
		<meta content="">
		<style>
			body {
				color: #a3aaaa;
				background-color: #253137;
			}

			a {
				color: #689dd7;
				text-decoration: none;
			}
			a:visited {
				color: #689dd7;
			}

			table {
				*border-collapse: collapse;
				border-spacing: 0;
			}
			table, td {
				border: 1px #3c6d68 solid;
			}
			tr {
				font-size: 2vh;
			}
			table {
				border-collapse: separate;
				border: solid 1px;
				border-radius: 6px;
				-moz-border-radius: 6px;
			}
			th {
				border-top: none;
			}
			td:first-child,
			th:first-child {
				border-left: none;
			}

			pre {
				color: #bcc0bc;
				background-color: #29363d;
				border: 1px black solid;
				padding: 1vw;
			}
		</style>
	</head>
	<body>
		<!-- A litle bit of list hackery --!>
		<ol start='0'>
			<li style='list-style-type:none'>
				<h1>
					Repository:
					<a href='https://${repo}'>
					   ${repo}
					</a>
				</h1>
				<pre>
					Commits: ${commits}
					First: ${first_commit_date}
					Last: ${last_commit_date}
					Changes: ${changes}
" > "${html_file}"


# Begin git log

git log --cc --pretty=format:"
				</pre>
			</li>
			<li>
				<table>
					<tr>
						<th>Date</th>
						<th>Hash</th>
						<th>Message</th>
						<th>Author</th>
						<th>Sig</th>
					</tr>
					<tr>
						<td>
							&nbsp; %as &nbsp;
						</td>
						<td>
							<a href='https://${repo}/commit/%H'>
							   &nbsp; %h &nbsp;
							</a>
						</td>
						<td>
							&nbsp; %s &nbsp;
						</td>
						<td>
							<a href='mailto:%aE'>
							   &nbsp; %an &nbsp;
							</a>
						</td>
						<td>
							&nbsp; %G? &nbsp;
						</td>
					</tr>
				</table>
				<pre>
	" >> "${html_file}"


# End

echo "
				</pre>
			</li>
		</ol>
	</body>
</html>
" >> "${html_file}"


echo "HTML file in: ${html_file}"
