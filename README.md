# Secret Service

<p align="center">
    <a href="https://gitlab.com/xgqt/secret_service/pipelines">
        <img src="https://gitlab.com/xgqt/secret_service/badges/master/pipeline.svg">
    </a>
    <a href="https://gitlab.com/xgqt/secret_service/commits/master.atom">
        <img src="https://img.shields.io/badge/feed-atom-orange.svg">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/badge/license-ISC-blue.svg">
    </a>
</p>


## Contents

- gitlab avatars
- licenses
- maintenance scripts
- templates


## Bootstrap

Install dependencies: bash curl git

```bash
curl https://gitlab.com/xgqt/secret_service/-/raw/master/create_source.sh | bash
cd ~/source/public/gitlab.com/xgqt
curl https://gitlab.com/xgqt/secret_service/-/raw/master/clone_all_the_things.sh | bash
```


# License

## Unless otherwise stated contents here are under the GNU GPL v3 license

This file is part of secret_service.

secret_service is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 secret_service is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with secret_service.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2020, XGQT
Licensed under the GNU GPL v3 License
